/* --COPYRIGHT--,BSD
 * Copyright (c) 2012, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/
#include "std_nhet.h"  

HET_MEMORY const HET_INIT0_PST[59] = 
{ 

 	/* hetlabel_0_0 */ 
	{
		0x00002801, 
		0x02880026, 
		0x00000000, 
		0x00000000
	}, 

 	/* hetlabel_1_0 */ 
	{
		0x00005A00, 
		0x00034038, 
		0x00000000, 
		0x00000000
	}, 

 	/* COMPARE_0 */ 
	{
		0x00006819, 
		0x02C10026, 
		0x00000000, 
		0x00000000
	}, 

 	/* FINISH_0 */ 
	{
		0x0000DA00, 
		0x00008028, 
		0x00000000, 
		0x00000000
	}, 

 	/* CSOFF_0 */ 
	{
		0x0000BE02, 
		0x0000BE04, 
		0xFFFFFF80, 
		0x00000000
	}, 

 	/* CLKOFF_0 */ 
	{
		0x00001E00, 
		0x00001404, 
		0x00000000, 
		0x00000000
	}, 

 	/* StartSPI_0 */ 
	{
		0x0000F480, 
		0x00020006, 
		0x00001100, 
		0x00000000
	}, 

 	/* CS0_LOW_0 */ 
	{
		0x00011E01, 
		0x00011E04, 
		0x00000000, 
		0x00000000
	}, 

 	/* DELAYCS_0 */ 
	{
		0x00001480, 
		0x00012006, 
		0x00000100, 
		0x00000000
	}, 

 	/* L00_0 */ 
	{
		0x00014CA0, 
		0x00000001, 
		0x00000000, 
		0x00000000
	}, 

 	/* SPICLK_0 */ 
	{
		0x00018000, 
		0x0041745C, 
		0x00000080, 
		0x00000000
	}, 

 	/* SPIMO_0 */ 
	{
		0x0001FE01, 
		0x0001EF04, 
		0x13D80000, 
		0x00000000
	}, 

 	/* L02_0 */ 
	{
		0x0001FA00, 
		0x0001A080, 
		0x00000000, 
		0x00000000
	}, 

 	/* SPIMI_0 */ 
	{
		0x0001DE09, 
		0x0001DF04, 
		0x00000000, 
		0x00000000
	}, 

 	/* INT_0 */ 
	{
		0x0001FE08, 
		0x0001F405, 
		0x007FFF80, 
		0x00000000
	}, 

 	/* End_0 */ 
	{
		0x00001A00, 
		0x00000000, 
		0x00000000, 
		0x00000000
	}, 

 	/* CS0_HI_0 */ 
	{
		0x00023E02, 
		0x00023E04, 
		0xFFFFFF00, 
		0x00000000
	}, 

 	/* WAIT_0 */ 
	{
		0x00001480, 
		0x00024006, 
		0x00000200, 
		0x00000000
	}, 

 	/* BUFF2_0 */ 
	{
		0x0002680B, 
		0x0000000E, 
		0x80000000, 
		0x00000000
	}, 

 	/* BUFF1_0 */ 
	{
		0x00028812, 
		0x0000000E, 
		0x180D0000, 
		0x00000000
	}, 

 	/* BUFF0_0 */ 
	{
		0x0002A813, 
		0x0000000E, 
		0x80000000, 
		0x00000000
	}, 

 	/* BUFF_0 */ 
	{
		0x0002C815, 
		0x0000000E, 
		0x80000000, 
		0x00000000
	}, 

 	/* SPI_0 */ 
	{
		0x0002E806, 
		0x0000000E, 
		0x00001100, 
		0x00000000
	}, 

 	/* UPWAIT_0 */ 
	{
		0x00030811, 
		0x0000000E, 
		0x00000200, 
		0x00000000
	}, 

 	/* UPDELAY_0 */ 
	{
		0x00032808, 
		0x0000000E, 
		0x00000100, 
		0x00000000
	}, 

 	/* COUNTER_0 */ 
	{
		0x00000C20, 
		0x00000004, 
		0x00000000, 
		0x00000000
	}, 

 	/* Lm00_0 */ 
	{
		0x00036CA0, 
		0x0000003D, 
		0x00000000, 
		0x00000000
	}, 

 	/* Lm11_0 */ 
	{
		0x00047A00, 
		0x00038080, 
		0x00000000, 
		0x00000000
	}, 

 	/* Lm12_0 */ 
	{
		0x0003B480, 
		0x00046000, 
		0x00000000, 
		0x00000000
	}, 

 	/* UPD_Am1_0 */ 
	{
		0x0003C824, 
		0x00000008, 
		0x00000F80, 
		0x00000000
	}, 

 	/* UPD_Cm1_0 */ 
	{
		0x0003E827, 
		0x00000008, 
		0x00000F80, 
		0x00000000
	}, 

 	/* UPD_Am2_0 */ 
	{
		0x00040825, 
		0x00000008, 
		0x00000F80, 
		0x00000000
	}, 

 	/* UPD_Cm2_0 */ 
	{
		0x00042828, 
		0x00000008, 
		0x00000F80, 
		0x00000000
	}, 

 	/* UPD_Am3_0 */ 
	{
		0x00044826, 
		0x00000008, 
		0x00000F80, 
		0x00000000
	}, 

 	/* UPD_Cm3_0 */ 
	{
		0x00046829, 
		0x00000008, 
		0x00000F80, 
		0x00000000
	}, 

 	/* TRIG1_0 */ 
	{
		0x00048000, 
		0x0044F15C, 
		0x00000F80, 
		0x00000000
	}, 

 	/* Lm15_0 */ 
	{
		0x0004A000, 
		0x0044A014, 
		0x00000F80, 
		0x00000000
	}, 

 	/* Lm25_0 */ 
	{
		0x0004C000, 
		0x0044C414, 
		0x00000F80, 
		0x00000000
	}, 

 	/* Lm35_0 */ 
	{
		0x00054000, 
		0x00454814, 
		0x00000F80, 
		0x00000000
	}, 

 	/* Lm16_0 */ 
	{
		0x00050000, 
		0x00450004, 
		0x00000F80, 
		0x00000000
	}, 

 	/* Lm26_0 */ 
	{
		0x00052000, 
		0x00452404, 
		0x00000F80, 
		0x00000000
	}, 

 	/* Lm36_0 */ 
	{
		0x00054000, 
		0x00454804, 
		0x00000F80, 
		0x00000000
	}, 

 	/* IND1_0 */ 
	{
		0x00059440, 
		0x00057516, 
		0x00000000, 
		0x00000000
	}, 

 	/* CLRCNT1_0 */ 
	{
		0x00058834, 
		0x00000008, 
		0x00000000, 
		0x00000000
	}, 

 	/* AFE1_0 */ 
	{
		0x0005DA00, 
		0x0005B920, 
		0x00000000, 
		0x00000000
	}, 

 	/* AFL1_0 */ 
	{
		0x0006BA00, 
		0x00069BC0, 
		0x00000000, 
		0x00000000
	}, 

 	/* ARE1_0 */ 
	{
		0x00061A00, 
		0x0005F940, 
		0x00000000, 
		0x00000000
	}, 

 	/* ARL1_0 */ 
	{
		0x0006BA00, 
		0x00069BE0, 
		0x00000000, 
		0x00000000
	}, 

 	/* BFE1_0 */ 
	{
		0x00065A00, 
		0x00063B20, 
		0x00000000, 
		0x00000000
	}, 

 	/* BFL1_0 */ 
	{
		0x0006BA00, 
		0x000699E0, 
		0x00000000, 
		0x00000000
	}, 

 	/* BRE1_0 */ 
	{
		0x00073A00, 
		0x00067B40, 
		0x00000000, 
		0x00000000
	}, 

 	/* BRL1_0 */ 
	{
		0x0006BA00, 
		0x000699C0, 
		0x00000000, 
		0x00000000
	}, 

 	/* FOR1_0 */ 
	{
		0x00072CE0, 
		0x00000FA0, 
		0x00000000, 
		0x00000000
	}, 

 	/* BAC1_0 */ 
	{
		0x0006C834, 
		0x00000018, 
		0x00000000, 
		0x00000000
	}, 

 	/* LIM1_0 */ 
	{
		0x00070100, 
		0x0006F900, 
		0x00000000, 
		0x00000000
	}, 

 	/* HIL1_0 */ 
	{
		0x00072834, 
		0x00000038, 
		0x0007D000, 
		0x00000000
	}, 

 	/* SSUB1_0 */ 
	{
		0x00072834, 
		0x00000038, 
		0xFFFFFF80, 
		0x00000000
	}, 

 	/* TRIG_HTU0_0 */ 
	{
		0x00074000, 
		0x0807591C, 
		0x00000100, 
		0x00000000
	}, 

 	/* TRIG_HTU1_0 */ 
	{
		0x00800000, 
		0x08001B1C, 
		0x00001900, 
		0x00000000
	} 
}; 
