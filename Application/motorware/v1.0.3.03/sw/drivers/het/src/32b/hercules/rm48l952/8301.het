;---------------------------------Variable Definitions--------------------------
;-------------------------------------------------------------------------------
MAXC      .equ  61		; 61 Default PWM base period (will be overwritten by CPU)
ADCT      .equ  31		; 31 Default compare value for ADC trigger pulse generation (will be overwritten by CPU)

;-----------------------------------------------Boost PWM Pair 1 Default Parameters--------------------------
;if Am=0, Cm=60, it generate 100% PWM because PWM_10 will NOT get cleared. 

;if Am=30LRP-1HRP, Cm=30LRP+1HRP, it generate the min duty cycle before reaching 0% PWM.
;if Am=30LRP, Cm=30LRP, it generate the 0% PWM. Keep increasing Am and decreasing Cm, PWM_mx1 will become 100% too

Am1        .equ  31		; 31 Default compare value (will be overwritten by CPU)
Cm1        .equ  31		; 31 Default compare value (will be overwritten by CPU)

Am2        .equ  31		; 31 Default compare value (will be overwritten by CPU)
Cm2        .equ  31		; 31 Default compare value (will be overwritten by CPU)

Am3        .equ  31		; 31 Default compare value (will be overwritten by CPU)
Cm3        .equ  31		; 31 Default compare value (will be overwritten by CPU)

;-----------------------------------------------Pin Usage for Motor PWM Pair 1--------------------------
PWM_m10    .equ  0		; PWM signal 10, PWM signal for half bridge of phase U
PWM_m20    .equ  4		; PWM signal 20, PWM signal for half bridge of phase V
PWM_m30    .equ  8		; PWM signal 30, PWM signal for half bridge of phase W
Pin_A1     .equ  25		; Signal A from QEP encoder 
Pin_B1     .equ  27		; Signal B from QEP encoder 
INDEX1     .equ  21		; Index from QEP encoder
INT_TRIG1  .equ  17		; HET pin for ADC trigger (TMS570LS20216)

Am4        .equ  31		; Default compare value (will be overwritten by CPU)
Cm4        .equ  31		; Default compare value (will be overwritten by CPU)

Am5        .equ  31		; Default compare value (will be overwritten by CPU)
Cm5        .equ  31		; Default compare value (will be overwritten by CPU)

Am6        .equ  31		; Default compare value (will be overwritten by CPU)
Cm6        .equ  31		; Default compare value (will be overwritten by CPU)

;-----------------------------------------------Pin Usage for Motor PWM Pair 1--------------------------
RST_m10    .equ  2		; Reset signal 10, RST signal for half bridge of phase U
RST_m20    .equ  6		; Reset signal 20, RST signal for half bridge of phase V
RST_m30    .equ  10		; Reset signal 30, RST signal for half bridge of phase W
Pin_A2     .equ  15		; Signal A from QEP encoder 
Pin_B2     .equ  31		; Signal B from QEP encoder 
INDEX2     .equ  20		; Index from QEP encoder
INT_TRIG2  .equ  17		; HET pin for ADC trigger (TMS570LS20216)

;----------------------------------------------- Control codes to start SPI or PWM -------------
;                                                Data = 0 will start SPI
;                                                Data = 1 will start PWM

START    SUB   {src1 = IMM, src2 = ZERO, dest=NONE, data=0, hr_data=0}
         BR    {event=NZ, cond_addr= Lm00, next=COMPARE}

COMPARE  SUB {src1=REM, src2=IMM, data=0, hr_data=0, remote=COUNTER, dest=NONE}
FINISH   BR  {event=Z, cond_addr=CSOFF, next=StartSPI}
CSOFF    SHFT { next=CLKOFF,smode=OR1,prv=OFF,cond_addr=CLKOFF,cond=UNC,pin=30,reg=T,irq=OFF,data=0x1FFFFFF};
CLKOFF   SHFT { next=START,smode=OR0,prv=OFF,cond_addr=START,cond=UNC,pin=20,reg=T,irq=OFF,data=0x0};
;----------------------------------------------- SPI codes -------------------------------------

StartSPI   DJZ { next=CS0_LOW,cond_addr=CS0_HI,reg=NONE,irq=OFF,data=34};
CS0_LOW SHFT { next=DELAYCS,smode=OL0,prv=OFF,cond_addr=DELAYCS,cond=UNC,pin=30,reg=T,irq=OFF,data=0x0};
DELAYCS DJZ   {next=START, cond_addr=L00, reg=NONE, irq=OFF, data=2}
L00     CNT     { next= SPICLK, reg = T, max= 1, irq = OFF,data=0}    
 
SPICLK    MCMP    { next= L02, cond_addr= SPIMO, hr_lr=HIGH, en_pin_action=ON, pin=20,order=REG_GE_DATA, action=PULSEHI, reg= T, data=1, hr_data=0}
 
       ; Comment for SPIMO
       ; assume user wants to transfer out 0x5A5A = 0101 1010 0101 1010
       ; but the HET codes will shift out MSB first, this will be arrange in the HET codes as 25-bit data as
       ; 0101 1010 0101 1010 0000 0000 0
       ; thus it becomes 0x0B4B400.  this is the values to be program to data field in order to transmit 0x5A5A
       ; DRV8301 Reg1 Write 0x13D8 => 0001 0011 1101 1000 0000 0000 0 => 0x027B000
       ; DRV8301 Reg1 Read  0x8000 => 1000 0000 0000 0000 0000 0000 0 => 0x1000000
       ; DRV8301 Reg1 Write 0x180D => 0001 1000 0000 1101 0000 0000 0 => 0x0301A00
       ; DRV8301 Reg1 Read  0x8000 => 1000 0000 0000 0000 0000 0000 0 => 0x1000000

SPIMO SHFT { next=END,smode=OL0,prv=OFF,cond_addr=END,cond=UNC,pin=15,reg=T,irq=OFF,data=0x027B000};
 
L02 BR      { next= END, cond_addr= SPIMI, event = ZERO}
 
SPIMI SHFT { next=INT,smode=ILL,prv=OFF,cond_addr=INT,cond=UNC,pin=31,reg=T,irq=OFF,data=0x0};
INT   SHFT { next=END,smode=IRM,prv=OFF,cond_addr=END,cond=UNC,pin=20,reg=T,irq=ON,data=0x0FFFF};
End   BR { next=Start,cond_addr=Start,event=NOCOND}; 

        
CS0_HI SHFT { next=WAIT,smode=OR1,prv=OFF,cond_addr=WAIT,cond=UNC,pin=30,reg=T,irq=OFF,data=0x1FFFFFE};
WAIT   DJZ  {next=START, cond_addr=BUFF2, reg=NONE, irq=OFF, data=4}
     ; program the next SPI words to be transferred here
BUFF2 MOV32 {type=IMTOREG&REM, reg=NONE, remote=SPIMO, data=0x1000000, hr_data=0}
     ; program the next SPI words to be transferred here
BUFF1 MOV32 {type=IMTOREG&REM, reg=NONE, remote=BUFF2, data=0x0301A00, hr_data=0}
     ; program the next SPI words to be transferred here
BUFF0 MOV32 {type=IMTOREG&REM, reg=NONE, remote=BUFF1, data=0x1000000, hr_data=0}

BUFF  MOV32 {type=IMTOREG&REM, reg=NONE, remote=BUFF, data=0x1000000, hr_data=0}
SPI   MOV32 {type=IMTOREG&REM, reg=NONE, remote=StartSPI, data=34, hr_data=0}
UPWAIT MOV32 {type=IMTOREG&REM, reg=NONE, remote=WAIT, data=4, hr_data=0}
UPDELAY MOV32 {type=IMTOREG&REM, reg=NONE, remote=DELAYCS, data=2, hr_data=0}
COUNTER CNT   { next= START, reg = A, max= 4, irq = OFF, data=0}

;-----------------------------------------------Boost PWM Pair 1 Start--------------------------

Lm00     CNT     { next= Lm11,  reg = T, max= MAXC, irq = OFF,data=0}           ; Counter -> Periode / Value on REG T

Lm11     BR      { next= TRIG1, cond_addr= Lm12, event = ZERO}         ; Branch for Updating Signal

Lm12   DJZ { next=UPD_Am1,cond_addr=TRIG1,reg=A,irq=OFF,data=0} ;To Guarantee Integrity of updating data. 
;Whenever CPU finishes updating Am1, Cm1, DB, write a '1' to the datafield of DJZ to update.

;-------------------------------------------Branch, Update the PWM parameters------------------------------

UPD_Am1   MOV32 { next=UPD_Cm1,remote=Lm15,type=IMTOREG&REM,reg=A,data=Am1,hr_data=0};
UPD_Cm1   MOV32 { next=UPD_Am2,remote=Lm16,type=IMTOREG&REM,reg=A,data=Cm1,hr_data=0};

UPD_Am2   MOV32 { next=UPD_Cm2,remote=Lm25,type=IMTOREG&REM,reg=A,data=Am2,hr_data=0};
UPD_Cm2   MOV32 { next=UPD_Am3,remote=Lm26,type=IMTOREG&REM,reg=A,data=Cm2,hr_data=0};

UPD_Am3   MOV32 { next=UPD_Cm3,remote=Lm35,type=IMTOREG&REM,reg=A,data=Am3,hr_data=0};
UPD_Cm3   MOV32 { next=TRIG1,remote=Lm36,type=IMTOREG&REM,reg=A,data=Cm3,hr_data=0};



;---------------------------------------------TRIG------------------------------

TRIG1    MCMP    { next= Lm15, cond_addr= Lm16, hr_lr=HIGH, en_pin_action=ON, pin=INT_TRIG1,order=REG_GE_DATA, action=PULSEHI, reg= T, data=ADCT, hr_data=0}

;-------------------------------------------Deal with the falling edge of PWM_mx1 and rising edge of PWM_mx0-------

;------------------------------------------- Motor PWM Pair 1----------------------------
Lm15     ECMP    { next= Lm25, cond_addr= Lm25, hr_lr=HIGH, en_pin_action=ON, pin=PWM_m10, action=SET, reg= T, data=Am1, hr_data=0};
;------------------------------------------- Motor PWM Pair 2----------------------------
Lm25     ECMP    { next= Lm35, cond_addr= Lm35, hr_lr=HIGH, en_pin_action=ON, pin=PWM_m20, action=SET, reg= T, data=Am2, hr_data=0};
;------------------------------------------- Motor PWM Pair 3----------------------------
Lm35     ECMP    { next= IND1, cond_addr= IND1, hr_lr=HIGH, en_pin_action=ON, pin=PWM_m30, action=SET, reg= T, data=Am3, hr_data=0};

;-------------------------------------------Deal with the rising edge of PWM_mx1 and falling edge of PWM_mx0-------

;------------------------------------------- Motor PWM Pair 1----------------------------
Lm16     ECMP    { next= Lm26, cond_addr= Lm26, hr_lr=HIGH, en_pin_action=ON, pin=PWM_m10, action=CLEAR, reg= T, data=Cm1, hr_data=0} 
;------------------------------------------- Motor PWM Pair 2----------------------------
Lm26     ECMP    { next= Lm36, cond_addr= Lm36, hr_lr=HIGH, en_pin_action=ON, pin=PWM_m20, action=CLEAR, reg= T, data=Cm2, hr_data=0} 
;------------------------------------------- Motor PWM Pair 3----------------------------
Lm36     ECMP    { next= IND1, cond_addr= IND1, hr_lr=HIGH, en_pin_action=ON, pin=PWM_m30, action=CLEAR, reg= T, data=Cm3, hr_data=0} 




;---------------------------------------------QEP-------------------------------
;---------------------------INDEX-CLR Counter ----------------------------------
;IND1     ECNT    { next = AFE1, cond_addr = CLRCNT1, event = Rise, pin = INDEX1, reg = NONE, data = 0} ;for anaheim motor
IND1     ECNT    { next = AFE1, cond_addr = CLRCNT1, event = Fall, pin = INDEX1, reg = NONE, data = 0} ;for teknic motor

CLRCNT1  MOV32   { next = AFE1, remote = FOR1, type = IMTOREG&REM, reg = A, Data = 0}

;---------------------------check: aFbL  /   aFbH ------------------------------

AFE1     BR      { next = ARE1, cond_addr = AFL1, event = Fall, pin = Pin_A1}

AFL1     BR      { next =  BAC1, cond_addr = FOR1, event = Low, pin = Pin_B1}

;---------------------------check: aRbL  /   aRbH ------------------------------

ARE1     BR      { next = BFE1, cond_addr = ARL1, event = rise, pin = Pin_A1}

ARL1     BR      { next = BAC1, cond_addr = FOR1, event = high, pin = Pin_B1}

;---------------------------check: bFaL  /   bFaH ------------------------------

BFE1     BR      { next = BRE1, cond_addr = BFL1, event = Fall, pin = Pin_B1}

BFL1     BR      { next = BAC1, cond_addr = FOR1, event = high, pin = Pin_A1}

;---------------------------check: bRaL  /   bRaH ------------------------------

BRE1     BR      { next = TRIG_HTU0, cond_addr = BRL1, event = rise, pin = Pin_B1}

BRL1     BR      { next =BAC1, cond_addr = FOR1, event = low, pin = Pin_A1}

;-------------------------------------------------------------------------------

;FOR1     CNT     { next = TRIG_HTU0, reg = NONE, max = 0x2000} ;for anaheim motor
FOR1     CNT     { next = TRIG_HTU0, reg = NONE, max = 0x0FA0} ;for teknic motor  

;-------------------------------------------------------------------------------

BAC1     MOV32   { next = LIM1, remote = FOR1, type = REMTOREG, reg = A}

LIM1     ECMP    { next= SSUB1, cond_addr= HIL1, hr_lr=LOW, en_pin_action=off, pin=CC25, reg= A, data=0}

;HIL1     ADM32   { next = TRIG_HTU0, remote = FOR1, type = IM&REGTOREM, reg = A, data = 0x0002000} ;for anaheim motor
HIL1     ADM32   { next = TRIG_HTU0, remote = FOR1, type = IM&REGTOREM, reg = A, data = 0x0000FA0} ;for teknic motor

SSUB1     ADM32   { next = TRIG_HTU0, remote = FOR1, type = IM&REGTOREM, reg = A, data = 0x1FFFFFF}



;-----------------------------------------------Boost PWM Pair 1 Start--------------------------

;Lm20     CNT     { next= Lm21,  reg = T, max= MAXC, irq = OFF,data=31}           ; Counter -> Periode / Value on REG T

;Lm21   BR { next=IND2,cond_addr=Lm22,event=ZERO};

;Lm22   DJZ { next=UPD_Am4,cond_addr=IND2,reg=A,irq=OFF,data=0} ;To Guarantee Integrity of updating data. 
;Whenever CPU finishes updating Am4, Cm4, DB, write a '1' to the datafield of DJZ to update.

;-------------------------------------------Branch, Update the PWM parameters------------------------------

;UPD_Am4   MOV32 { next=UPD_Cm4,remote=Lm45,type=IMTOREG&REM,reg=A,data=Am4,hr_data=0};
;UPD_Cm4   MOV32 { next=UPD_Am5,remote=Lm46,type=IMTOREG&REM,reg=A,data=Cm4,hr_data=0};

;UPD_Am5   MOV32 { next=UPD_Cm5,remote=Lm55,type=IMTOREG&REM,reg=A,data=Am5,hr_data=0};
;UPD_Cm5   MOV32 { next=UPD_Am6,remote=Lm56,type=IMTOREG&REM,reg=A,data=Cm5,hr_data=0};

;UPD_Am6   MOV32 { next=UPD_Cm6,remote=Lm65,type=IMTOREG&REM,reg=A,data=Am6,hr_data=0};
;UPD_Cm6   MOV32 { next=IND2,remote=Lm66,type=IMTOREG&REM,reg=A,data=Cm6,hr_data=0};


;---------------------------------------------QEP-------------------------------
;---------------------------INDEX-CLR Counter ----------------------------------
;IND2     ECNT    { next = AFE2, cond_addr = CLRCNT2, event = Rise, pin = INDEX2, reg = NONE, data = 0}

;CLRCNT2  MOV32   { next = AFE2, remote = FOR2, type = IMTOREG&REM, reg = A, Data = 0}

;---------------------------check: aFbL  /   aFbH ------------------------------

;AFE2     BR      { next = ARE2, cond_addr = AFL2, event = Fall, pin = Pin_A2}

;AFL2     BR      { next =  BAC2, cond_addr = FOR2, event = Low, pin = Pin_B2}

;---------------------------check: aRbL  /   aRbH ------------------------------

;ARE2     BR      { next = BFE2, cond_addr = ARL2, event = rise, pin = Pin_A2}

;ARL2     BR      { next = BAC2, cond_addr = FOR2, event = high, pin = Pin_B2}

;---------------------------check: bFaL  /   bFaH ------------------------------

;BFE2     BR      { next = BRE2, cond_addr = BFL2, event = Fall, pin = Pin_B2}

;BFL2     BR      { next = BAC2, cond_addr = FOR2, event = high, pin = Pin_A2}

;---------------------------check: bRaL  /   bRaH ------------------------------

;BRE2     BR      { next = TRIG2, cond_addr = BRL2, event = rise, pin = Pin_B2}

;BRL2     BR      { next =BAC2, cond_addr = FOR2, event = low, pin = Pin_A2}

;-------------------------------------------------------------------------------

;FOR2     CNT     { next = TRIG2, reg = NONE, max = 0x1F40}

;-------------------------------------------------------------------------------

;BAC2     MOV32   { next = LIM2, remote = FOR2, type = REMTOREG, reg = A}

;LIM2     ECMP    { next= SSUB2, cond_addr= HIL2, hr_lr=LOW, en_pin_action=off, pin=CC27, reg= A, data=0}

;HIL2     ADM32   { next = TRIG2, remote = FOR2, type = IM&REGTOREM, reg = A, data = 0x0001F40}

;SSUB2     ADM32   { next = TRIG2 , remote = FOR2, type = IM&REGTOREM, reg = A, data = 0x1FFFFFF}


;---------------------------------------------TRIG------------------------------

;TRIG2    MCMP    { next= Lm45, cond_addr= Lm46, hr_lr=HIGH, en_pin_action=ON, pin=INT_TRIG2,order=REG_GE_DATA, action=PULSEHI, reg= T, data=ADCT, hr_data=0}

;-------------------------------------------Deal with the falling edge of PWM_mx1 and rising edge of PWM_mx0-------

;------------------------------------------- Motor PWM Pair 1----------------------------
;Lm45     ECMP    { next= Lm55, cond_addr= Lm55, hr_lr=HIGH, en_pin_action=ON, pin=PWM_m40, action=SET, reg= T, data=Am4, hr_data=0};
;------------------------------------------- Motor PWM Pair 2----------------------------
;Lm55     ECMP    { next= Lm65, cond_addr= Lm65, hr_lr=HIGH, en_pin_action=ON, pin=PWM_m50, action=SET, reg= T, data=Am5, hr_data=0};
;------------------------------------------- Motor PWM Pair 3----------------------------
;Lm65     ECMP    { next= TRIG_HTU0, cond_addr= TRIG_HTU0, hr_lr=HIGH, en_pin_action=ON, pin=PWM_m60, action=SET, reg= T, data=Am6, hr_data=0};

;-------------------------------------------Deal with the rising edge of PWM_mx1 and falling edge of PWM_mx0-------

;------------------------------------------- Motor PWM Pair 1----------------------------;
;Lm46     ECMP    { next= Lm56, cond_addr= Lm56, hr_lr=HIGH, en_pin_action=ON, pin=PWM_m40, action=CLEAR, reg= T, data=Cm4, hr_data=0} 
;------------------------------------------- Motor PWM Pair 2----------------------------
;Lm56     ECMP    { next= Lm66, cond_addr= Lm66, hr_lr=HIGH, en_pin_action=ON, pin=PWM_m50, action=CLEAR, reg= T, data=Cm5, hr_data=0} 
;------------------------------------------- Motor PWM Pair 3----------------------------
;Lm66     ECMP    { next= TRIG_HTU0, cond_addr= TRIG_HTU0, hr_lr=HIGH, en_pin_action=ON, pin=PWM_m60, action=CLEAR, reg= T, data=Cm6, hr_data=0} 
TRIG_HTU0   ECMP { next=TRIG_HTU1, reqnum=0,request=GENREQ, hr_lr=HIGH,en_pin_action=OFF,cond_addr=TRIG_HTU1,pin=25,action=PULSEHI,reg=T,data=2,hr_data=0};
TRIG_HTU1   ECMP { next=START, reqnum=1,request=GENREQ, hr_lr=HIGH,en_pin_action=OFF,cond_addr=START,pin=27,action=PULSEHI,reg=T,data=50,hr_data=0};



